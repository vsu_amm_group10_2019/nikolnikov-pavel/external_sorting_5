﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace ext_sort
{
    public partial class MainForm : Form
    {
        string file_Name="";
        const int kol_vo = 10;
        const int mnogo = 50;
        const int FilesCount = 4;
        string otvet = "Ответ.txt";
        string input = "Пример.txt";
        public MainForm()
        {
            InitializeComponent();
        }

        private void uslovie_Click(object sender, EventArgs e)
        {//что надо сдлеать
            MessageBox.Show("Вывести названия конфет и их изготовителей в порядке увеличения срокахранения конфет." +
                "\nИспользовать многопутевое однофазное естественное несбалансированное слияние.",
                "Задача 1e",
                MessageBoxButtons.OK,
                MessageBoxIcon.Question);

        }
        
        private void CreateMenu_Click(object sender, EventArgs e)
        {//создание файла
            using (FileForm file = new FileForm(input))
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не создан.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else if (!File.Exists(file.fileName) ||
                    MessageBox.Show("Файл существует. Перезаписать?", "Warning", 
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    StreamWriter newFile = new StreamWriter(file.fileName);
                    newFile.Close();
                    MessageBox.Show("Файл создан.", "Information",
                                MessageBoxButtons.OK,  MessageBoxIcon.Information);
                }
                else
                    MessageBox.Show("Отмена создания файла.", "Warning",
                                MessageBoxButtons.OK,   MessageBoxIcon.Warning);
            }

        }

        private void OpenMenu_Click(object sender, EventArgs e)
        {//открытие существующего файла
            using (FileForm file = new FileForm(input))
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена создания файла.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное имя файла. Файл не открыт.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error); 
                else if (!File.Exists(file.fileName))
                    MessageBox.Show("Файл не существует.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    file_Name = file.fileName;                    
                    intxt.LoadFile(file_Name, RichTextBoxStreamType.PlainText);
                    outtxt.Text = "";
                    MessageBox.Show("Файл открыт.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }
        
        private void SaveMenu_Click(object sender, EventArgs e)
        {//сохранение файла
            if (file_Name != "")
            {
                intxt.SaveFile(file_Name, RichTextBoxStreamType.PlainText);
                MessageBox.Show("Файл сохранён.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Файл не сохранён, т.к. он не открыт.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
        }

        private void AddMenu_Click(object sender, EventArgs e)
        {//добавление в открытый файл
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            using (CandiesForm file = new CandiesForm())
            {
                DialogResult result = file.ShowDialog();
                if (result == DialogResult.Cancel)
                    MessageBox.Show("Отмена добавления элемента.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                else if (result == DialogResult.No)
                    MessageBox.Show("Некорректное значение элемента. Элемент не добавлен.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    Candies candy = file.Candy;
                    intxt.Text += candy.ToString() + Environment.NewLine;
                    MessageBox.Show("Элемент добавлен.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

        }
        private void AddTenMenu_Click(object sender, EventArgs e)
        {//генерация 10 записей
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }            
            for (int i=0; i<kol_vo; i++)
            {
                CandiesForm file = new CandiesForm(kol_vo);
               
                Candies candy = file.Candy;               

                intxt.Text += candy.ToString() + Environment.NewLine;
            }
            intxt.SaveFile(file_Name, RichTextBoxStreamType.PlainText);
        }
        private void Add50Menu_Click(object sender, EventArgs e)
        {//генерация 50 записей
            if (file_Name == "")
            {
                MessageBox.Show("Необходимо открыть файл.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            for (int i = 0; i < mnogo; i++)
            {
                CandiesForm file = new CandiesForm(mnogo);

                Candies candy = file.Candy;

                intxt.Text += candy.ToString() + Environment.NewLine;
            }
            intxt.SaveFile(file_Name, RichTextBoxStreamType.PlainText);

        }

        private void SortMenu_Click(object sender, EventArgs e)
        {//сортировка открытого файла
            if (file_Name == "")
                MessageBox.Show("Необходимо открыть файл.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
            else
            {
                using (FileForm file = new FileForm(otvet))
                {                    
                    if (file.ShowDialog() != DialogResult.OK)
                        MessageBox.Show("Некорректное имя файла. Файл не создан.", "Error",
                                MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        if (!File.Exists(file.fileName) ||
                            MessageBox.Show("Файл существует. Перезаписать?", "Warning",
                                MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                        
                        {
                            MergeSort(file_Name, file.fileName);
                            MessageBox.Show("Файл отcортирован.", "Information",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            intxt.LoadFile(file_Name, RichTextBoxStreamType.PlainText);                            
                            outtxt.LoadFile(file.fileName, RichTextBoxStreamType.PlainText);
                        }
                        else
                            MessageBox.Show("Отмена создания файла для сортировки.", "Warning",
                                MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                }
            }
        }

        void MergeSort(string inputfileName, string outputFileName)
        {
            string[] fileNames = new string[FilesCount];
            for (int j = 0; j < FilesCount; j++) // создаем вспомогательные файлы
            {
                fileNames[j] = 1 + j + ".txt";
                StreamWriter f = new StreamWriter(fileNames[j]);                
                f.Close();
            }            
            bool isSorted = MergeSerieses(inputfileName, fileNames[2], fileNames[0], fileNames[1]);
            int Index = 0; 
            while (!isSorted)
            {// пока файл не отсортирован
                if (Index == 0) 
                    isSorted = MergeSerieses(fileNames[0], fileNames[1], fileNames[2], fileNames[3]);
                else 
                    isSorted = MergeSerieses(fileNames[2], fileNames[3], fileNames[0], fileNames[1]);
                Index += 2;
                Index %= 4;
            }
            File.Copy(fileNames[Index], outputFileName, true); // сохраняем результат
            for (int j = 0; j < FilesCount; j++) 
                // удаляем вспомогательные файлы
                File.Delete(fileNames[j]);
        }
        bool MergeSerieses(string f1_in, string f2_in, string f1_out, string f2_out)
        {
            StreamReader f1_read = new StreamReader(f1_in);
            StreamReader f2_read = new StreamReader(f2_in);
            StreamWriter[] f_write = new StreamWriter[] { new StreamWriter(f1_out), new StreamWriter(f2_out) };

            Candies lastRead1 = null;
            Candies lastRead2 = null;
            string info = f1_read.ReadLine();
            if (info != null)
                lastRead1 = new Candies(info);
            info = f2_read.ReadLine();
            if (info != null)
                lastRead2 = new Candies(info);
            int i = 0; 
            while (lastRead1 != null || lastRead2 != null)
            {
                Merge(f1_read, f2_read, f_write[i % 2], ref lastRead1, ref lastRead2);
                i++;
            }
            f1_read.Close();
            f2_read.Close();
            f_write[0].Close();
            f_write[1].Close();
            return i <= 1;
        }

        /// <summary>
        /// сливаем крайнии серии из файлов
        /// </summary>
        /// <param name="f1_in"> первый файл, содержащий серию </param>
        /// <param name="f2_in"> второй файл, содержащий серию </param>
        /// <param name="f_out"> файл, в который сливаются серии </param>
        /// <param name="lastRead1"> последний считанный из файла fr1 элемент </param>
        /// <param name="lastRead2"> последний считанный из файла fr2 элемент </param>
        void Merge(StreamReader f1_in, StreamReader f2_in, StreamWriter f_out, ref Candies lastRead1, ref Candies lastRead2)
        {
            bool isSeriesF1 = lastRead1 != null; // файл содержит серию, если он не пуст
            bool isSeriesF2 = lastRead2 != null;
            while (isSeriesF1 && isSeriesF2)
            {// пока что оба файла сожержат серии
                if (lastRead1.CompareTo(lastRead2) <= 0)
                    // элемент первого файла меньше второго
                    isSeriesF1 = AddFromFile(f1_in, f_out, ref lastRead1);
                else
                    isSeriesF2 = AddFromFile(f2_in, f_out, ref lastRead2);
            }
            if (isSeriesF1) 
                while (AddFromFile(f1_in, f_out, ref lastRead1)) ;
            else if (isSeriesF2)
                while (AddFromFile(f2_in, f_out, ref lastRead2)) ; 
        }

        /// <summary>
        /// записываем в файл последний считанный элемент
        /// </summary>
        /// <param name="f_in"> файл, из которого считан lastRead</param>
        /// <param name="f_out"> файл, в который записываем lastRead</param>
        /// <param name="lastRead"> последний считанный элемент из файла </param>
        /// <returns> возвращаем true, если в файле, из которого считали элемент, продолжается текущая серия </returns>
        bool AddFromFile(StreamReader f_in, StreamWriter f_out, ref Candies lastRead)
        {
            f_out.WriteLine(lastRead); // записываем последний считанный элемент в файл
            Candies lastWritten = lastRead; // запоминаем последний записанный элемент
            string info = f_in.ReadLine(); // считывает следующий элемент из файла file_read
            if (info == null)
            {
                lastRead = null;
                return false; 
            }            
            lastRead = new Candies(info);            
            return lastRead.CompareTo(lastWritten) >= 0;
        }

        
    }
}
