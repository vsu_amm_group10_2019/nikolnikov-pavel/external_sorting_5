﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ext_sort
{
    public partial class CandiesForm : Form
    {
        public Candies Candy { get; private set; }
        public CandiesForm()
        {
            InitializeComponent();
            Candy = null;
            DialogResult = DialogResult.None;
        }

        public CandiesForm(int n)
        {
            InitializeComponent();
            ten();
        }
        private void button_auto_Click(object sender, EventArgs e)
        {//автозаполнение полей
            if (Candy == null)
            {
                Candy = new Candies();
            }
            Candy.GenerateRandom();
            Display();
        }

        private void button_create_Click(object sender, EventArgs e)
        {//создание записи
            bool isCorrect = tbxName.Text != "" && tbxWeight.Text != "" && 
                tbxPrice.Text != "" && tbxManufacturer.Text != "" && tbxexpirationDate.Text != "";
            if (!isCorrect)
            {
                Candy = null;
                DialogResult = DialogResult.No;
            }
            else
            {
                Read();
                DialogResult = DialogResult.Yes;
            }
        }
        void ten()
        {
            if (Candy == null)
            {
                Candy = new Candies();
            }
            Candy.GenerateRandom();
            Candy.ToString();
            Display();
            Read();
           // DialogResult = DialogResult.Yes;
        }
        void Display()
        {//вывод записи
            if (Candy != null)
            {
                tbxName.Text = Candy.Name;
                tbxWeight.Text = Candy.Weight.ToString();
                tbxPrice.Text = Candy.Price.ToString();
                tbxManufacturer.Text = Candy.Manufacturer;
                dtpDate.Value = Candy.DateOfManufacture;
                tbxexpirationDate.Text = Candy.expirationDate.ToString();
            }
        }

        void Read()
        {//занасение значений
            Candy = new Candies();
            Candy.Name = tbxName.Text;
            Candy.Weight = Convert.ToInt32(tbxWeight.Text);
            Candy.Price = Convert.ToInt32(tbxPrice.Text);
            Candy.Manufacturer = tbxManufacturer.Text;
            Candy.DateOfManufacture = dtpDate.Value;
            Candy.expirationDate = Convert.ToInt32(tbxexpirationDate.Text);
        }

    }
}
