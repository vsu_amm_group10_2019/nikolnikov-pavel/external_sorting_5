﻿
namespace ext_sort
{
    partial class MainForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.menu = new System.Windows.Forms.MenuStrip();
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.CreateMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.OpenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.SaveMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.doMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.AddMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.SortMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.uslovie = new System.Windows.Forms.ToolStripMenuItem();
            this.openFile = new System.Windows.Forms.OpenFileDialog();
            this.saveFile = new System.Windows.Forms.SaveFileDialog();
            this.intxt = new System.Windows.Forms.RichTextBox();
            this.outtxt = new System.Windows.Forms.RichTextBox();
            this.AddTenMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.Add50Menu = new System.Windows.Forms.ToolStripMenuItem();
            this.menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // menu
            // 
            this.menu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.doMenu,
            this.uslovie});
            this.menu.Location = new System.Drawing.Point(0, 0);
            this.menu.Name = "menu";
            this.menu.Size = new System.Drawing.Size(1031, 24);
            this.menu.TabIndex = 0;
            this.menu.Text = "menuStrip1";
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.CreateMenu,
            this.OpenMenu,
            this.SaveMenu});
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(48, 20);
            this.fileMenu.Text = "Файл";
            // 
            // CreateMenu
            // 
            this.CreateMenu.Name = "CreateMenu";
            this.CreateMenu.Size = new System.Drawing.Size(133, 22);
            this.CreateMenu.Text = "Создать";
            this.CreateMenu.Click += new System.EventHandler(this.CreateMenu_Click);
            // 
            // OpenMenu
            // 
            this.OpenMenu.Name = "OpenMenu";
            this.OpenMenu.Size = new System.Drawing.Size(133, 22);
            this.OpenMenu.Text = "Открыть";
            this.OpenMenu.Click += new System.EventHandler(this.OpenMenu_Click);
            // 
            // SaveMenu
            // 
            this.SaveMenu.Name = "SaveMenu";
            this.SaveMenu.Size = new System.Drawing.Size(133, 22);
            this.SaveMenu.Text = "Сохранить";
            this.SaveMenu.Click += new System.EventHandler(this.SaveMenu_Click);
            // 
            // doMenu
            // 
            this.doMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddMenu,
            this.SortMenu,
            this.AddTenMenu,
            this.Add50Menu});
            this.doMenu.Name = "doMenu";
            this.doMenu.Size = new System.Drawing.Size(70, 20);
            this.doMenu.Text = "Действие";
            // 
            // AddMenu
            // 
            this.AddMenu.Name = "AddMenu";
            this.AddMenu.Size = new System.Drawing.Size(188, 22);
            this.AddMenu.Text = "Добавить запись";
            this.AddMenu.Click += new System.EventHandler(this.AddMenu_Click);
            // 
            // SortMenu
            // 
            this.SortMenu.Name = "SortMenu";
            this.SortMenu.Size = new System.Drawing.Size(188, 22);
            this.SortMenu.Text = "Отсортировать";
            this.SortMenu.Click += new System.EventHandler(this.SortMenu_Click);
            // 
            // uslovie
            // 
            this.uslovie.Name = "uslovie";
            this.uslovie.Size = new System.Drawing.Size(65, 20);
            this.uslovie.Text = "Условие";
            this.uslovie.Click += new System.EventHandler(this.uslovie_Click);
            // 
            // openFile
            // 
            this.openFile.FileName = "openFileDialog1";
            // 
            // intxt
            // 
            this.intxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.intxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.intxt.Location = new System.Drawing.Point(0, 27);
            this.intxt.Name = "intxt";
            this.intxt.Size = new System.Drawing.Size(510, 637);
            this.intxt.TabIndex = 1;
            this.intxt.Text = "";
            // 
            // outtxt
            // 
            this.outtxt.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.outtxt.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.outtxt.Location = new System.Drawing.Point(516, 27);
            this.outtxt.Name = "outtxt";
            this.outtxt.Size = new System.Drawing.Size(510, 637);
            this.outtxt.TabIndex = 2;
            this.outtxt.Text = "";
            // 
            // AddTenMenu
            // 
            this.AddTenMenu.Name = "AddTenMenu";
            this.AddTenMenu.Size = new System.Drawing.Size(188, 22);
            this.AddTenMenu.Text = "Добавить 10 записей";
            this.AddTenMenu.Click += new System.EventHandler(this.AddTenMenu_Click);
            // 
            // Add50Menu
            // 
            this.Add50Menu.Name = "Add50Menu";
            this.Add50Menu.Size = new System.Drawing.Size(188, 22);
            this.Add50Menu.Text = "Добавить 50 записей";
            this.Add50Menu.Click += new System.EventHandler(this.Add50Menu_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Info;
            this.ClientSize = new System.Drawing.Size(1031, 663);
            this.Controls.Add(this.outtxt);
            this.Controls.Add(this.intxt);
            this.Controls.Add(this.menu);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menu;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1047, 702);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(1047, 702);
            this.Name = "MainForm";
            this.Text = "Сортировка Конфет";
            this.menu.ResumeLayout(false);
            this.menu.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menu;
        private System.Windows.Forms.OpenFileDialog openFile;
        private System.Windows.Forms.SaveFileDialog saveFile;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem CreateMenu;
        private System.Windows.Forms.ToolStripMenuItem OpenMenu;
        private System.Windows.Forms.ToolStripMenuItem SaveMenu;
        private System.Windows.Forms.ToolStripMenuItem doMenu;
        private System.Windows.Forms.ToolStripMenuItem AddMenu;
        private System.Windows.Forms.ToolStripMenuItem SortMenu;
        private System.Windows.Forms.ToolStripMenuItem uslovie;
        private System.Windows.Forms.RichTextBox intxt;
        private System.Windows.Forms.RichTextBox outtxt;
        private System.Windows.Forms.ToolStripMenuItem AddTenMenu;
        private System.Windows.Forms.ToolStripMenuItem Add50Menu;
    }
}

