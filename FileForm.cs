﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ext_sort
{
    public partial class FileForm : Form
    {
        public FileForm(string str)
        {
            InitializeComponent();
            DialogResult = DialogResult.None;
            textBox1.Text = str;
        }
        public string fileName
        {//возвращаем название
            get { return textBox1.Text; }
        }

        private void button_ok_Click(object sender, EventArgs e)
        {//проверка на зарезервированные файлы для работы
            bool isCorrectFileName = fileName != "1.txt" && fileName != "2.txt" &&
                fileName != "" && fileName != "3.txt" && fileName != "4.txt";
            if (isCorrectFileName) { DialogResult = DialogResult.OK; }
            else { DialogResult = DialogResult.No; }                
            Close();
        }

        private void button_back_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
